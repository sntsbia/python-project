package com.python.application

import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.chaquo.python.PyException
import com.chaquo.python.Python
import com.chaquo.python.android.AndroidPlatform


class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (!Python.isStarted()) {
            Python.start(AndroidPlatform(this))
        }

        val py = Python.getInstance()
        val deteccaoMod = py.getModule("deteccao_vacas")
        val reconhecimentoMod = py.getModule("reconhecimentorn_teste_vacas")


        findViewById<Button>(R.id.btn_action).setOnClickListener {
            try {
//               deteccaoMod.callAttr("mainFunction")
                reconhecimentoMod.callAttr("mainFunction")
            } catch (e: PyException) {
                e.printStackTrace()
                Toast.makeText(this, e.message, Toast.LENGTH_LONG).show()
            }
        }
    }
}