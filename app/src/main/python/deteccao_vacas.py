import os
import dlib
import cv2
import glob
from os.path import dirname,join

def mainFunction():
    #print(dlib.test_simple_object_detector("fotos/fotos_william/vacas_faces_01.xml", "recursos/detector_vacas.svm"))
    # Primeiro modelo precision: 0.0769231
    # "fotos/fotos_william/vacas_faces_01.xml" 50% em fotos faceis
    filename = join(dirname(__file__),"recursos","detector_vacas_faces_v13_cel.svm")

    directory = join(dirname(__file__),"reduzidas")

    detectorRelogio=dlib.simple_object_detector(filename)
    total=0
    totalDetectados=0
    #for imagem in glob.glob(os.path.join("C:/python/teste_classificador_vaquinhas/positivas_v0/", "*.png")):
    #for imagem in glob.glob(os.path.join("fotos/fotos_william/treinamento_pontos/", "*.png")):
    #for imagem in glob.glob(os.path.join("fotos/fotos_william/teste_modelos/", "*.png")):
    #for imagem in glob.glob(os.path.join("C:/python/teste_classificador_vaquinhas/REM CABALLERO/","*.png")):
    # fase 2
    for imagem in glob.glob(os.path.join(directory, "*.jpg")):
    #for imagem in glob.glob(os.path.join("C:/python/imagens_cel/originais/parla/", "*.jpg")):
    #for imagem in glob.glob(os.path.join("C:/python/imagens_cel/outras_imagens_cel/", "*.jpeg")):
    #for imagem in glob.glob(os.path.join("C:/python/imagens_bia/image/", "*.jpg")):

        #print("Arquivo: ",imagem)

        total=total+1
        img = cv2.imread(imagem)
        objetosDetectados=detectorRelogio(img,1)
        #print("Faces detectadas: ",imagem, len(objetosDetectados))
        totalDetectados=totalDetectados+len(objetosDetectados)
        for d in objetosDetectados:
            e, t, d, b = (int(d.left()), int(d.top()), int(d.right()), int(d.bottom()))
            cv2.rectangle(img, (e,t), (d, b), (0,0,255), 2)
        if  len(objetosDetectados) < 1:
            print("Nao achei nenhuma face no arquivo " ,imagem )
        else:
            print("Encontrou: ",objetosDetectados, " faces em ", imagem)

    print ("total de imagens :",total, "\nFaces detectadas no total:",totalDetectados)



